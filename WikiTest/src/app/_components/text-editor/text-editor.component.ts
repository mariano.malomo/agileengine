import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { WordModel } from './style.model';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.css']
})
export class TextEditorComponent implements OnInit {
  @Input() updateSelectedWord: EventEmitter<string> = new EventEmitter();
  @Output() onWordSelected: EventEmitter<string> = new EventEmitter();

  public model: any = {};
  private words: Array<WordModel>;

  constructor() {
    this.words = new Array<WordModel>();
  }

  public getWords(text: string) {
    let words = text.split(' ');

    this.words.length = 0;
    words.forEach((w: string) => {
      this.words.push(new WordModel(w));
    });
  }

  public onTextSelection() {
    let selection = window.getSelection().toString().trim();

    let enableBtns = selection.length > 0 && !selection.includes(' ');

    this.model.enableBoldStyle = enableBtns;
    this.model.enableItalicStyle = enableBtns;
    this.model.enableUnderlineStyle = enableBtns;

    let word = this.words.find(w => w.word == selection);

    if (word) {
      this.model.hasBold = word.styles.includes('bold');
      this.model.hasItalic = word.styles.includes('italic');
      this.model.hasUnderline = word.styles.includes('underline');
    }

    if (selection && selection.length > 0) {
      this.onWordSelected.emit(selection);
    }
  }

  public toggleStyle(style: string) {
    let word = window.getSelection().toString().trim();

    let savedWord = this.words.find(w => w.word == word);

    if (savedWord.styles.includes(style)) {
      savedWord.styles.splice(savedWord.styles.indexOf(style), 1);
    } else {
      savedWord.styles.push(style);
    }
    console.log(this.words);
    this.applyStyles();
  }

  ngOnInit() {
    if (this.updateSelectedWord) {
      this.updateSelectedWord
        .subscribe(newWord => {
          let selection = window.getSelection().toString().trim();
          let word = this.words.find(w => w.word == selection);

          word.word = newWord;
          this.applyStyles();
        });
    }
  }

  private applyStyles() {
    let tmpText = '';

    this.words.forEach(s => {
      tmpText += `${s.styledWord} `;
    });

    this.model.editableText = tmpText;
  }

}
