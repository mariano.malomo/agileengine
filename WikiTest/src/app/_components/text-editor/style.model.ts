export class WordModel {
    word: string;
    index: number;

    styles: string[];

    constructor(word: string) {
        this.word = word.trim();
        this.styles = [];
    }

    get styledWord(): string {
        let styledWord = this.word;

        this.styles.forEach(s => {
            switch (s) {
                case 'bold':
                    styledWord = `<span class='bold'>${styledWord || this.word}</span>`;
                    break;
                case 'italic':
                    styledWord = `<span class='italic'>${styledWord || this.word}</span>`;
                    break;
                case 'underline':
                    styledWord = `<span class='underline'>${styledWord || this.word}</span>`;
                    break;
            }
        });

        return styledWord;
    }
}
