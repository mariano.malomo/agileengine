import { Component, EventEmitter } from '@angular/core';
import { SynonymsService } from './_services/synonyms.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public updateWord: EventEmitter<string> = new EventEmitter();
  public syns: any;

  title = 'WikiTest';

  constructor(private synService: SynonymsService) { }

  public onWordSelected(word: string) {
    this.syns = this.synService.getSyns(word);
  }

  public changeWord(word: string) {
    this.updateWord.emit(word);
  }
}
