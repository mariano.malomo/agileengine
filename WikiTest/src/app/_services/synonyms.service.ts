import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { orderBy } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class SynonymsService {

  constructor(private http: HttpClient) { }

  public getSyns(word: string) {
    return this.http.get(`https://api.datamuse.com/words?ml=${word}`)
      .pipe(
        map((res: Array<any>) => orderBy(res, (i) => i.score, 'desc')),
        map((res: Array<any>) => res.splice(0, 5)));
  }
}
